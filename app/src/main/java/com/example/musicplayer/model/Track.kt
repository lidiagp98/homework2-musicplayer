package com.example.musicplayer.model

import android.graphics.Bitmap

class Track (val name: String?, val artist: String?, val album: String?, val duration: String?,val year: String?, val coverImage: Bitmap?)
