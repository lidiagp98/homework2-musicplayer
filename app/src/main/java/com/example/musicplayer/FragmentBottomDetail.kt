package com.example.musicplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class FragmentBottomDetail : BottomSheetDialogFragment() {

    lateinit var songName: TextView
    lateinit var artist: TextView
    lateinit var album: TextView
    lateinit var duration: TextView
    lateinit var year: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.detail_bottom_fragment, container, false)
        songName = view.findViewById(R.id.dtName)
        artist = view.findViewById(R.id.dtArtist)
        album = view.findViewById(R.id.dtAlbum)
        year = view.findViewById(R.id.dtYear)
        duration = view.findViewById(R.id.dtDuration)
        songName.text = arguments?.getString("name")
        artist.text = arguments?.getString("artist")
        album.text = arguments?.getString("album")
        year.text = arguments?.getString("year")
        duration.text = arguments?.getString("duration")
        return view
    }
}