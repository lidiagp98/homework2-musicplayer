package com.example.musicplayer

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.musicplayer.databinding.ActivityMainBinding
import com.example.musicplayer.service.MusicService

class MainActivity : AppCompatActivity(), ServiceConnection {

    companion object {
        private const val ONE_SECOND = 1000
        private const val ZERO = 0
    }

    lateinit var binding: ActivityMainBinding
    var musicService: MusicService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        callService()
        musicListenersControl()
    }

    private fun musicListenersControl() {
        binding.playPause.setOnClickListener {
            musicService?.playPauseSong()
        }
        binding.nextBtn.setOnClickListener {
            musicService?.nextTrack()
            setMetaDataInView()
        }
        binding.backBtn.setOnClickListener {
            musicService?.previousTrack()
            setMetaDataInView()
        }

        details()

        changeSongAutomatically()

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) musicService?.seekTo(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    private fun prepareSeekBar() {
        refreshTotalTime()
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed(object : Runnable {
            override fun run() {
                try {
                    binding.seekBar.progress = musicService?.currentPosition() ?: 0
                    binding.elapsedTime.text =
                        musicService?.createTimeLabels(musicService?.currentPosition() ?: 0)
                    handler.postDelayed(this, ONE_SECOND.toLong())
                } catch (e: Exception) {
                    binding.seekBar.progress = 0
                }
            }
        }, ZERO.toLong())
    }

    private fun refreshTotalTime() {
        binding.seekBar.max = musicService?.totalTime ?: 0
        binding.totalTime.text = musicService?.createTimeLabels(musicService?.totalTime ?: 0)
    }

    private fun details() {
        binding.detailBtn.setOnClickListener {
            val dialog = FragmentBottomDetail()
            val bundle = Bundle()
            dialog.arguments = musicService?.prepareBundleInformation(bundle)
            val transaction = supportFragmentManager.beginTransaction()
            dialog.show(transaction, "")
        }
    }

    private fun changeSongAutomatically() {
        musicService?.changeSongAutomatically()
        setMetaDataInView()
    }

    private fun setMetaDataInView() {
        binding.coverImage.setImageBitmap(musicService?.track?.coverImage)
        binding.songName.text = musicService?.track?.name
        binding.artistName.text = musicService?.track?.artist
        binding.albumName?.text = musicService?.track?.album
    }

    override fun onResume() {
        val intent = Intent(applicationContext, MusicService::class.java)
        bindService(intent, this, BIND_AUTO_CREATE)
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        unbindService(this)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        musicService = null
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        var musicBinder = service as MusicService.MusicBinder
        musicService = musicBinder.getMusicService()
        musicService?.trackIndex?.observe(this, Observer {
            changeSongAutomatically()
            refreshTotalTime()
        })
        musicService?.playPauseBtn?.observe(this, Observer {
            if (it) binding.playPause.setImageResource(R.drawable.ic_pause_circle)
            else binding.playPause.setImageResource(R.drawable.ic_play_circle_filled)
        })
        if (musicService?.isPlaying() == true) musicService?.popNotification(R.drawable.ic_pause_circle)
        setMetaDataInView()
        prepareSeekBar()
    }

    private fun callService() {
        val intent = Intent(this, MusicService::class.java)
        startService(intent)
        musicService?.popNotification(R.drawable.ic_play_circle_filled)
    }
}