package com.example.musicplayer.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.musicplayer.application.ApplicationC.Companion.NEXT_ACTION
import com.example.musicplayer.application.ApplicationC.Companion.PLAY_ACTION
import com.example.musicplayer.application.ApplicationC.Companion.PREVIOUS_ACTION
import com.example.musicplayer.service.MusicService

class NotificationReceiver() : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action
        val serviceIntent = Intent(context, MusicService::class.java)
        action?.let {
            when (action) {
                PLAY_ACTION -> {
                    serviceIntent.putExtra("Action", "Play/Pause")
                    context?.startService(serviceIntent)
                }
                NEXT_ACTION -> {
                    serviceIntent.putExtra("Action", "Next Song")
                    context?.startService(serviceIntent)
                }
                PREVIOUS_ACTION -> {
                    serviceIntent.putExtra("Action", "Previous Song")
                    context?.startService(serviceIntent)
                }
                else -> {
                }
            }
        }
    }

}