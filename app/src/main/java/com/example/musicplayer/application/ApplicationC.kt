package com.example.musicplayer.application

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class ApplicationC : Application() {
    companion object {
        const val CHANNEL_ID_1 = "channel1"
        const val PREVIOUS_ACTION = "previousaction"
        const val NEXT_ACTION = "actionnext"
        const val PLAY_ACTION = "playaction"
    }

    override fun onCreate() {
        super.onCreate()
        prepareNotificationChannel()
    }

    private fun prepareNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val firstChannel =
                NotificationChannel(CHANNEL_ID_1, "Channel_1", NotificationManager.IMPORTANCE_HIGH)
            firstChannel.description = "This is the primary channel"
            val manager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(firstChannel)
        }
    }
}