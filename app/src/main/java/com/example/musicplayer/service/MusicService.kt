package com.example.musicplayer.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import com.example.musicplayer.MainActivity
import com.example.musicplayer.R
import com.example.musicplayer.application.ApplicationC
import com.example.musicplayer.broadcast.NotificationReceiver
import com.example.musicplayer.model.Track

open class MusicService : Service() {

    companion object {
        private const val PLAY_PREVIOUS_SONG = 1
        private const val PLAY_NEXT_SONG = 2
        private const val SECONDS = 1000
        private const val MINUTES = 60
        private const val ZERO = 0
        private const val TEN = 10
        private const val PACKAGE = "android.resource://com.example.musicplayer/"
        const val MEDIA_PLAYER_INITIALIZED = "INITIALIZED"
        const val MEDIA_PLAYER_NOT_INITIALIZED = "NOT INITIALIZED"
        const val FOREGROUND_ID = 9999
    }

    private lateinit var mediaSessionCompat: MediaSessionCompat
    private val musicBinder: IBinder = MusicBinder()
    private lateinit var mediaPlayer: MediaPlayer
    private val listOfSongs = listOf(R.raw.loverboy, R.raw.otherside, R.raw.waitaminute)
    var totalTime = 0
    lateinit var track: Track
    private var currentSong = 0
    private val metadataRetriever = MediaMetadataRetriever()
    var trackIndex = MutableLiveData<Int>()
    var playPauseBtn = MutableLiveData<Boolean>()
    private var mediaPlayerIsInitialized = MEDIA_PLAYER_NOT_INITIALIZED

    override fun onBind(intent: Intent?): IBinder? = musicBinder
    inner class MusicBinder : Binder() {
        fun getMusicService() = this@MusicService
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (mediaPlayerIsInitialized == MEDIA_PLAYER_NOT_INITIALIZED) {
            mediaSessionCompat = MediaSessionCompat(baseContext, "Music Player Session")
            mediaPlayer = MediaPlayer.create(this, listOfSongs[currentSong])
            totalTime = mediaPlayer.duration
            extractMetadata(listOfSongs[currentSong])
            changeSongAutomatically()
            mediaPlayerIsInitialized = MEDIA_PLAYER_INITIALIZED
        }
        val action = intent?.getStringExtra("Action")
        action?.let {
            when (action) {
                "Play/Pause" -> {
                    playPauseSong()
                }
                "Next Song" -> {
                    nextTrack()
                }
                "Previous Song" -> {
                    previousTrack()
                }
                else -> {
                }
            }
        }
        return START_NOT_STICKY
    }

    fun isPlaying() = mediaPlayer.isPlaying

    fun seekTo(progress: Int) = mediaPlayer.seekTo(progress)

    fun currentPosition() = mediaPlayer.currentPosition

    fun playPauseSong() {
        if (mediaPlayer.isPlaying) {
            popNotification(R.drawable.ic_play_circle_filled)
            mediaPlayer.pause()
            playPauseBtn.value = mediaPlayer.isPlaying
        } else {
            popNotification(R.drawable.ic_pause_circle)
            mediaPlayer.start()
            playPauseBtn.value = mediaPlayer.isPlaying
        }
    }

    fun nextTrack() {
        resetMediaPlayer(mediaPlayer)
        changeSong(PLAY_NEXT_SONG)
        extractMetadata(listOfSongs[currentSong])
        popNotification(R.drawable.ic_pause_circle)
        totalTime = mediaPlayer.duration
        trackIndex.value = currentSong
        mediaPlayer.start()
        playPauseBtn.value = mediaPlayer.isPlaying
    }

    fun previousTrack() {
        resetMediaPlayer(mediaPlayer)
        changeSong(PLAY_PREVIOUS_SONG)
        extractMetadata(listOfSongs[currentSong])
        popNotification(R.drawable.ic_pause_circle)
        totalTime = mediaPlayer.duration
        trackIndex.value = currentSong
        mediaPlayer.start()
        playPauseBtn.value = mediaPlayer.isPlaying
    }

    private fun changeSong(action: Int) {
        if (action == PLAY_NEXT_SONG) {
            currentSong = if (currentSong < listOfSongs.size - 1) currentSong + 1 else ZERO
            mediaPlayer = MediaPlayer.create(this, listOfSongs[currentSong])
            totalTime = mediaPlayer.duration
        } else if (action == PLAY_PREVIOUS_SONG) {
            currentSong = if (currentSong > ZERO) currentSong - 1 else listOfSongs.size - 1
            mediaPlayer = MediaPlayer.create(this, listOfSongs[currentSong])
            totalTime = mediaPlayer.duration
        }
    }

    private fun resetMediaPlayer(mediaPlayer: MediaPlayer) {
        mediaPlayer.stop()
        mediaPlayer.release()
    }

    private fun extractMetadata(songId: Int) {
        val location = Uri.parse("${PACKAGE}$songId")
        metadataRetriever.setDataSource(this, location)
        val trackImage = metadataRetriever.embeddedPicture
        var trackBitmap: Bitmap? = null
        trackImage?.let {
            trackBitmap = BitmapFactory.decodeByteArray(trackImage, ZERO, trackImage.size)
        }
        track = Track(
            metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE),
            metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST),
            metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM),
            createTimeLabels(totalTime),
            metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR),
            trackBitmap
        )
    }

    fun prepareBundleInformation(bundle: Bundle): Bundle {
        bundle.putString("name", track.name)
        bundle.putString("artist", track.artist)
        bundle.putString("album", track.album)
        bundle.putString("year", track.year)
        bundle.putString("duration", track.duration)
        return bundle
    }

    fun createTimeLabels(time: Int): String {
        val minutes = time.div(SECONDS).div(MINUTES)
        val seconds = time.div(SECONDS).rem(MINUTES)
        return "$minutes:${if (seconds < TEN) "0$seconds" else seconds}"
    }

    fun changeSongAutomatically() {
        mediaPlayer.setOnCompletionListener {
            resetMediaPlayer(mediaPlayer)
            changeSong(PLAY_NEXT_SONG)
            extractMetadata(listOfSongs[currentSong])
            trackIndex.value = currentSong
            popNotification(R.drawable.ic_pause_circle)
            mediaPlayer.start()
            playPauseBtn.value = mediaPlayer.isPlaying
        }
    }

    fun popNotification(idPlayPauseBtn: Int) {
        val intent = Intent(this, MainActivity::class.java)
        val intentPending =
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val previousIntent =
            Intent(this, NotificationReceiver::class.java).setAction(ApplicationC.PREVIOUS_ACTION)
        val previousPending =
            PendingIntent.getBroadcast(this, 0, previousIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val pauseIntent =
            Intent(this, NotificationReceiver::class.java).setAction(ApplicationC.PLAY_ACTION)
        val pausePending =
            PendingIntent.getBroadcast(this, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val nextIntent =
            Intent(this, NotificationReceiver::class.java).setAction(ApplicationC.NEXT_ACTION)
        val nextPending =
            PendingIntent.getBroadcast(this, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(this, ApplicationC.CHANNEL_ID_1)
            .setSmallIcon(R.drawable.ic_music_)
            .setLargeIcon(track.coverImage)
            .setContentTitle(track.name)
            .setContentText(track.artist)
            .setContentIntent(intentPending)
            .addAction(R.drawable.ic_arrow_back, "Previous Song", previousPending)
            .addAction(idPlayPauseBtn, "Pause", pausePending)
            .addAction(R.drawable.ic_forward, "Next Song", nextPending)
            .setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(mediaSessionCompat.sessionToken)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOnlyAlertOnce(true)
            .build()

        if (!mediaPlayer.isPlaying) {
            startForeground(FOREGROUND_ID, notification)
        } else {
            startForeground(FOREGROUND_ID, notification)
            stopForeground(false)
        }
    }
}